import { PrismaClient } from "@prisma/client";
import { faker } from "@faker-js/faker";

const prisma = new PrismaClient();

async function main() {
  for (let i = 0; i < 10; i += 1) {
    await prisma.user.upsert({
      where: { email: faker.internet.email() },
      update: {},
      create: {
        email: faker.internet.email(),
        name: faker.person.firstName(),
        authId: faker.string.uuid(),
        products: {
          create: Array.from(
            { length: faker.number.int({ min: 3, max: 6 }) },
            () => ({
              name: faker.commerce.productName(),
              description: faker.commerce.productDescription(),
            })
          ),
        },
      },
    });
  }
}

try {
  await main();
  await prisma.$disconnect();
} catch (e) {
  console.error(e);
  await prisma.$disconnect();
  process.exit(1);
}
