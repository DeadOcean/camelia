import { connect } from "@dagger.io/dagger";
import { z } from "zod";
import { gql, GraphQLClient } from "graphql-request";

const GITLAB_TOKEN = process.env["GITLAB_TOKEN"];

const gitlabPipelineSchema = z
  .object({
    project: z.object({
      pipelines: z.object({
        nodes: z.array(
          z.object({
            id: z.string(),
            status: z.string(),
            ref: z.string(),
            commit: z.object({
              shortId: z.string(),
            }),
          }),
        ),
      }),
    }),
  })
  .transform((pipeline) =>
    pipeline.project.pipelines.nodes.map((pipeline) => ({
      id: pipeline.id,
      status: pipeline.status,
      ref: pipeline.ref,
      commit: pipeline.commit.shortId,
    })),
  );

const getLatestSuccessfulPipeline = async (
  projectPath: string,
  ref: string,
  source: string,
) => {
  const query = gql`
    {
      project(fullPath: "${projectPath}") {
        pipelines(status: SUCCESS, ref: "${ref}", source: "${source}", first: 1) {
          nodes {
            id
            status
            ref
            commit {
              shortId
            }
          }
        }
      }
    }
  `;

  const client = new GraphQLClient("https://gitlab.com/api/graphql", {
    headers: {
      Authorization: `Bearer ${GITLAB_TOKEN}`,
    },
  });

  const data = await client.request(query);

  return gitlabPipelineSchema.parse(data).at(0);
};

connect(
  async (client) => {
    const baseImage = "node:18-slim";
    const nodeCache = client.cacheVolume("node");

    const pipelineSource = process.env["CI_PIPELINE_SOURCE"] ?? "push";
    const targetBranch =
      process.env["CI_MERGE_REQUEST_TARGET_BRANCH_NAME"] ?? "main";
    const sourceBranch =
      process.env["CI_MERGE_REQUEST_SOURCE_BRANCH_NAME"] ?? "feature";

    const latestSuccessfulPipeline = await getLatestSuccessfulPipeline(
      process.env["CI_PROJECT_PATH"]!,
      process.env["CI_COMMIT_REF_NAME"]!,
      process.env["CI_PIPELINE_SOURCE"]!,
    );

    let filter: string;

    if (pipelineSource === "merge_request_event") {
      filter = `...[${targetBranch}...${sourceBranch}]`;
    } else if (latestSuccessfulPipeline) {
      filter = `...[HEAD^...${latestSuccessfulPipeline.commit}]`;
    } else {
      filter = "...[HEAD^1]";
    }

    const lint = client
      .container()
      .from(baseImage)
      .withMountedCache("/tmp/.pnpm-store", nodeCache)
      .withWorkdir("/app")
      .withExec(["apt-get", "-qq", "-o=Dpkg::Use-Pty=0", "update", "-y"])
      .withExec([
        "apt-get",
        "-qq",
        "-o=Dpkg::Use-Pty=0",
        "install",
        "openssl",
        "tree",
        "git",
        "-y",
      ])
      .withExec(["npm", "i", "-g", "turbo"])
      .withExec(["corepack", "enable"])
      .withExec(["corepack", "prepare", "pnpm@latest-8", "--activate"])
      .withExec(["pnpm", "config", "set", "store-dir", "/tmp/.pnpm-store"])
      .withMountedDirectory(
        ".",
        client.host().directory(".", {
          exclude: ["node_modules", "**/node_modules"],
        }),
      )
      .withExec(["git", "fetch"])
      .withExec(["pnpm", "install", "--frozen-lockfile"]);

    await lint
      .withExec(["turbo", "run", "lint:ci", `--filter=${filter}`])
      .sync();

    await lint
      .withExec(["turbo", "run", "format:ci", `--filter=${filter}`])
      .sync();

    await lint
      .withExec(["turbo", "run", "typecheck", `--filter=${filter}`])
      .sync();
  },
  { LogOutput: process.stderr, Workdir: "../.." },
);
