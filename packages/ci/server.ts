import { type Client } from "@dagger.io/dagger";

export const pipeline = async (client: Client) => {
  const baseImage = "node:18-slim";

  const builder = client
    .container()
    .from(baseImage)
    .withWorkdir("/app")
    .withExec(["npm", "i", "-g", "turbo"])
    .withMountedDirectory(
      ".",
      client.host().directory(".", {
        exclude: ["node_modules", "**/node_modules"],
      }),
    )
    .withExec(["turbo", "prune", "--scope=server", "--docker"]);

  const installer = client
    .container()
    .from(baseImage)
    .withExec(["apt", "-qq", "update", "-y"])
    .withExec(["apt", "-qq", "install", "openssl", "tree", "-y"])
    .withWorkdir("/app")
    .withExec(["npm", "i", "-g", "turbo"])
    .withExec(["corepack", "enable"])
    .withExec(["corepack", "prepare", "pnpm@latest-8", "--activate"])
    .withDirectory(".", builder.directory("/app/out/json"))
    .withFile("./pnpm-lock.yaml", builder.file("/app/out/pnpm-lock.yaml"))
    .withExec(["pnpm", "install"])
    .withDirectory(".", builder.directory("/app/out/full"))
    .withFile("turbo.json", client.host().file("turbo.json"))
    .withExec(["turbo", "run", "build", "--filter=server"])
    .withExec(["pnpm", "prune", "--prod"])
    .withEntrypoint(["pnpm", "--filter=server", "start"]);

  return installer;
};
