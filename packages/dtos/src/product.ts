import { Type } from "@sinclair/typebox";

export const createProduct = Type.Object(
  {
    name: Type.String(),
    description: Type.Union([Type.String(), Type.Null()]),
    active: Type.Boolean({ default: true }),
    addedAt: Type.Date(),
  },
  { additionalProperties: false }
);
