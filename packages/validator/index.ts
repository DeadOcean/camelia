import type { Static, TSchema } from "@sinclair/typebox";
import { TypeCompiler } from "@sinclair/typebox/compiler";
import { Value } from "@sinclair/typebox/value";

export const compileParse = <T extends TSchema>(schema: T) => {
  const check = TypeCompiler.Compile(schema);

  return (input: unknown): Static<T> => {
    const convert = Value.Convert(schema, input);
    const checked = check.Check(convert);

    if (checked) return convert;

    const { path, message } = check.Errors(convert).First()!;

    throw new Error(`${path} ${message}`);
  };
};

export default compileParse;
