import gracefulShutdown from "http-graceful-shutdown";

import { createApp } from "@/app.ts";
import env from "@/lib/env.ts";

const app = createApp();

const server = app.listen(env.PORT, () => {
  app.context.log.info(`Server listening on ${env.PORT} 🚀`);
});

gracefulShutdown(server, {
  forceExit: false,
  onShutdown: app.context.db.$disconnect,
});
