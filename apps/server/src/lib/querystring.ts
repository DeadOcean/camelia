import type { default as Koa, Next } from "koa";
import qs from "qs";

import type { Context } from "@/types.ts";

const querycache = new Map<string, any>();

export const patchQs = (app: Koa<Koa.DefaultState, Context>): void => {
  Object.defineProperty(app.request, "query", {
    get() {
      const str = this.querystring;

      if (!str) {
        return {};
      }

      let query = querycache.get(str);

      if (!query) {
        query = qs.parse(str);
        querycache.set(str, query);
      }

      return query;
    },
    set(obj: Record<string, any>) {
      this.querystring = qs.stringify(obj);
    },
  });
};

export const querystringMiddleware = async (ctx: Context, next: Next) => {
  if (ctx.request.querystring) {
    ctx.query = qs.parse(ctx.request.querystring);
  }
  await next();
};
