import { pinoHttp, type Options } from "pino-http";
import type { Next } from "koa";

import type { Context } from "@/types.ts";

export const loggerMiddleware = (
  opts?: Options,
  stream?: Options["stream"],
) => {
  const wrap = pinoHttp(opts, stream);
  const pino = async (ctx: Context, next: Next) => {
    wrap(ctx.req, ctx.res);
    ctx.log = ctx.request.log = ctx.response.log = ctx.req.log;

    try {
      return await next();
    } catch (err) {
      ctx.log.error({ err });
      throw err;
    }
  };
  pino.logger = wrap.logger;
  return pino;
};
