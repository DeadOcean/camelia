import {
  TypeRegistry,
  Kind,
  type TSchema,
  FormatRegistry,
} from "@sinclair/typebox";

export interface TStringEnum<T extends string[] = string[]> extends TSchema {
  [Kind]: "StringEnum";
  static: T[number];
  type: "string";
  enum: T;
}

TypeRegistry.Set<TStringEnum>("StringEnum", (schema, value) => {
  return typeof value === "string" && schema.enum.includes(value);
});

export const StringEnum = <T extends string[]>(
  values: [...T],
): TStringEnum<T> => {
  return {
    [Kind]: "StringEnum",
    type: "string",
    enum: values,
  } as TStringEnum<T>;
};

FormatRegistry.Set("uuid", (value) =>
  /^[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$/gi.test(
    value,
  ),
);
