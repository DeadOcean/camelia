import { Type } from "@sinclair/typebox";
import { compileParse } from "@camelia/validator";

import { StringEnum } from "./schemas.ts";

const envSchema = Type.Object({
  DATABASE_URL: Type.String(),
  PORT: Type.Number(),
  IP_FAMILY: Type.Union([Type.Literal(4), Type.Literal(6)]),
  NODE_ENV: StringEnum(["development", "production", "test"]),
  LOG_LEVEL: StringEnum(["trace", "debug", "info", "warn", "error", "fatal"]),
  CORS_ORIGINS: Type.String(),
  SUPERTOKENS_CORE_URI: Type.String(),
  SUPERTOKENS_APP_NAME: Type.String(),
  SUPERTOKENS_API_DOMAIN: Type.String(),
  SUPERTOKENS_WEBSITE_DOMAIN: Type.String(),
});

const loadEnv = compileParse(envSchema);

export default loadEnv(process.env);
