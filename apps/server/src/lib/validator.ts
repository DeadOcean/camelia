import type { Next } from "koa";
import Router from "@koa/router";
import type {
  Static,
  TAnySchema,
  TRecord,
  TSchema,
  TString,
} from "@sinclair/typebox";
import { compileParse } from "@camelia/validator";
import { verifySession } from "supertokens-node/recipe/session/framework/koa";

import type { Context } from "@/types.ts";

type TParsedUrlQuery = TRecord<TString, TAnySchema>;

type RequestValidationMiddlewareParams<
  TBody extends TSchema,
  TQuery extends TParsedUrlQuery,
> = {
  bodySchema?: TBody;
  querySchema?: TQuery;
};

export const requestValidationMiddleware = <
  TBody extends TSchema,
  TQuery extends TParsedUrlQuery,
>({
  bodySchema,
  querySchema,
}: RequestValidationMiddlewareParams<TBody, TQuery>) => {
  const bodyValidator = bodySchema ? compileParse(bodySchema) : null;
  const queryValidator = querySchema ? compileParse(querySchema) : null;

  return async (ctx: Context, next: Next) => {
    if (bodyValidator) {
      ctx.body = bodyValidator(ctx.request.body);
    } else {
      ctx.body = ctx.request.body;
    }

    if (queryValidator) {
      ctx.query = queryValidator(ctx.query);
    }

    await next();
  };
};

export const responseValidationMiddleware =
  <TResponse extends TSchema>(schema?: TResponse) =>
  async (ctx: Context, next: Next) => {
    if (schema) {
      const validator = compileParse(schema);
      ctx.response.body = validator(ctx.response.body);
    }
    await next();
  };

type Action<
  TReqSchema extends TSchema = TSchema,
  TResSchema extends TSchema = TSchema,
  TQuerySchema extends TParsedUrlQuery = TParsedUrlQuery,
> =
  | {
      method: "GET" | "DELETE";
      route: string;
      resSchema?: TResSchema;
      querySchema?: TQuerySchema;
      protected?: boolean;
      handler: (
        ctx: Context<
          Static<TReqSchema>,
          Static<TQuerySchema>,
          Static<TResSchema>
        >,
      ) => Static<TResSchema> | Promise<Static<TResSchema>>;
    }
  | {
      method: "POST" | "PUT" | "PATCH";
      route: string;
      resSchema?: TResSchema;
      reqSchema?: TReqSchema;
      querySchema?: TQuerySchema;
      protected?: boolean;
      handler: (
        ctx: Context<
          Static<TReqSchema>,
          Static<TQuerySchema>,
          Static<TResSchema>
        >,
      ) => Static<TResSchema> | Promise<Static<TResSchema>>;
    };

const handlerMiddleware = (
  handler: (ctx: Context) => unknown | Promise<unknown>,
) => {
  return async (ctx: Context, next: Next) => {
    ctx.response.body = await Promise.resolve(handler(ctx));
    await next();
  };
};

type ValidatedPipeline = [
  string,
  ...((ctx: Context, next: Next) => void | Promise<void>)[],
];

export const validatedAction = <
  TReqSchema extends TSchema = TSchema,
  TResSchema extends TSchema = TSchema,
  TQuerySchema extends TParsedUrlQuery = TParsedUrlQuery,
>(
  action: Action<TReqSchema, TResSchema, TQuerySchema>,
) => {
  const router = new Router();
  let pipeline: ValidatedPipeline;

  switch (action.method) {
    case "GET":
    case "DELETE":
      pipeline = [
        action.route,
        ...(action.protected ? [verifySession()] : []),
        requestValidationMiddleware({ querySchema: action.querySchema }),
        handlerMiddleware(action.handler),
        responseValidationMiddleware(action.resSchema),
      ];
      break;
    case "POST":
    case "PUT":
    case "PATCH":
      pipeline = [
        action.route,
        ...(action.protected ? [verifySession()] : []),
        requestValidationMiddleware({
          querySchema: action.querySchema,
          bodySchema: action.reqSchema,
        }),
        handlerMiddleware(action.handler),
        responseValidationMiddleware(action.resSchema),
      ];
      break;
  }

  router[action.method.toLowerCase() as Lowercase<Action["method"]>](
    ...pipeline,
  );

  return router;
};
