import type { init } from "supertokens-node";
import Session from "supertokens-node/recipe/session";
import EmailPassword from "supertokens-node/recipe/emailpassword";
import Dashboard from "supertokens-node/recipe/dashboard";
import type { PrismaClient } from "@camelia/database";
import type { Context } from "koa";

import env from "@/lib/env.ts";

export const envToLogger = {
  development: {
    level: "debug",
    transport: {
      target: "pino-pretty",
      options: {
        translateTime: "HH:MM:ss Z",
        ignore: "pid,hostname,ns,mod",
      },
    },
  },
  production: {
    level: env.LOG_LEVEL,
  },
  test: {
    level: "silent",
  },
};

export const supertokensConfig = (
  prisma: PrismaClient,
): Parameters<typeof init>[0] => ({
  framework: "koa",
  supertokens: {
    connectionURI: env.SUPERTOKENS_CORE_URI,
  },
  appInfo: {
    appName: env.SUPERTOKENS_APP_NAME,
    apiDomain: env.SUPERTOKENS_API_DOMAIN,
    websiteDomain: env.SUPERTOKENS_WEBSITE_DOMAIN,
    apiBasePath: "/auth",
    websiteBasePath: "/login",
  },
  recipeList: [
    EmailPassword.init({
      override: {
        functions: (originalImplementation) => ({
          ...originalImplementation,
          signUp: async (input) => {
            const response = await originalImplementation.signUp(input);

            if (response.status === "OK") {
              await prisma.user.create({
                data: { email: input.email, authId: response.user.id },
              });
            }
            return response;
          },
        }),
      },
    }),
    Session.init(),
    Dashboard.init(),
  ],
  telemetry: false,
});

export const corsOrigins = (ctx: Context) => {
  if (env.NODE_ENV === "production") {
    const origin = ctx.headers.origin;

    if (typeof origin === "undefined") {
      return "*";
    }

    if (env.CORS_ORIGINS.split(",").includes(origin)) {
      return origin;
    }

    return ctx.throw(`${origin} is not a valid origin!`);
  }
  return "*";
};
