import type { Options } from "pino-http";
import type {
  Context as BaseContext,
  Request as BaseRequest,
  Response as BaseResponse,
} from "koa";
import type { PrismaClient } from "@camelia/database";
import type { SessionContext } from "supertokens-node/framework/koa";

export interface Context<
  TBody = unknown,
  TQuery extends Record<string, any> = Record<string, any>,
  TRes = unknown,
> extends BaseContext {
  log: NonNullable<Options["logger"]>;
  request: Request<TBody, TQuery>;
  response: Response<TRes>;
  body: TBody;
  query: TQuery;
  db: PrismaClient;
  session?: SessionContext["session"];
}

export interface Request<TBody, TQuery> extends Omit<BaseRequest, "query"> {
  log: NonNullable<Options["logger"]>;
  body: TBody;
  query: TQuery;
}

export interface Response<TBody> extends BaseResponse {
  log: NonNullable<Options["logger"]>;
  body: TBody;
}
