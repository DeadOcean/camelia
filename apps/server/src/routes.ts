import { postsRouter } from "@/modules/posts/module.ts";
import Router from "@koa/router";

export const router = new Router();

router.use("/", postsRouter.routes());
