import Koa from "koa";
import { koaBody } from "koa-body";
import koaCors from "@koa/cors";
import supertokens from "supertokens-node";
import { middleware as supertokensMiddleware } from "supertokens-node/framework/koa";
import { PrismaClient } from "@camelia/database";

import { router } from "@/routes.ts";
import { loggerMiddleware } from "@/lib/logger.ts";
import env from "@/lib/env.ts";
import type { Context } from "@/types.ts";
import { patchQs, querystringMiddleware } from "@/lib/querystring.ts";
import {
  corsOrigins as origin,
  envToLogger,
  supertokensConfig,
} from "@/config.ts";

export const createApp = () => {
  const app = new Koa<Koa.DefaultState, Context>();
  const prisma = new PrismaClient();

  app.context.db = prisma;

  supertokens.init(supertokensConfig(prisma));

  patchQs(app);

  app.use(
    koaCors({
      origin,
      allowHeaders: ["content-type", ...supertokens.getAllCORSHeaders()],
      credentials: true,
    }),
  );

  const logger = loggerMiddleware(envToLogger[env.NODE_ENV]);

  app.use(logger);
  app.use(supertokensMiddleware());
  app.use(querystringMiddleware);
  app.use(koaBody());
  app.use(router.routes());

  app.context.log = logger.logger;

  return app;
};
