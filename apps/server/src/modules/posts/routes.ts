import Router from "@koa/router";

import { create } from "./actions/create.ts";
import { find } from "./actions/find.ts";

export const router = new Router({ prefix: "posts" });

router.use(create.routes());
router.use(find.routes());
