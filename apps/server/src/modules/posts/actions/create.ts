import { Type } from "@sinclair/typebox";
import { createProduct } from "@camelia/dtos";

import { validatedAction } from "@/lib/validator.ts";

export const create = validatedAction({
  method: "POST",
  route: "/",
  reqSchema: createProduct,
  resSchema: Type.Intersect([
    createProduct,
    Type.Object(
      { id: Type.Number(), userId: Type.Number() },
      { additionalProperties: false },
    ),
  ]),
  protected: true,
  handler: async (ctx) => {
    const user = await ctx.db.user.findFirst({
      where: { authId: ctx.session!.getUserId() },
    })!;
    const newProduct = await ctx.db.product.create({
      data: {
        ...ctx.body,
        userId: user!.id,
      },
    });
    return newProduct;
  },
});
