import { createProduct } from "@camelia/dtos";
import { Type } from "@sinclair/typebox";

import { validatedAction } from "@/lib/validator.ts";

export const find = validatedAction({
  method: "GET",
  route: "/",
  querySchema: Type.Record(Type.String(), Type.Any()),
  resSchema: Type.Array(
    Type.Intersect([
      createProduct,
      Type.Object(
        { id: Type.Number(), userId: Type.Number() },
        { additionalProperties: false },
      ),
    ]),
  ),
  handler: (ctx) => ctx.db.product.findMany({ take: 50 }),
});
