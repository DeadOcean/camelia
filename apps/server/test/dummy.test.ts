import { describe, test, expect } from "vitest";

describe("dummy test", () => {
  test("1 + 1 = 2", () => {
    expect(1 + 1).toEqual(2);
  });
});
