import SuperTokens, { SuperTokensWrapper } from "supertokens-auth-react";
import EmailPassword from "supertokens-auth-react/recipe/emailpassword";
import Session from "supertokens-auth-react/recipe/session";
import { getSuperTokensRoutesForReactRouterDom } from "supertokens-auth-react/ui";
import { EmailPasswordPreBuiltUI } from "supertokens-auth-react/recipe/emailpassword/prebuiltui";
import * as ReactRouterDom from "react-router-dom";
import { SessionAuth } from "supertokens-auth-react/recipe/session";

import { env } from "@/lib/env";
import { ThemeProvider } from "./lib/themeProvider";
import { lazy } from "react";

const Home = lazy(() => import("@/pages/home"));
const ProtectedPage = lazy(() => import("@/pages/protected"));

SuperTokens.init({
  appInfo: {
    appName: env.VITE_SUPERTOKENS_APP_NAME,
    apiDomain: env.VITE_SUPERTOKENS_API_DOMAIN,
    websiteDomain: env.VITE_SUPERTOKENS_WEBSITE_DOMAIN,
    apiBasePath: "/auth",
    websiteBasePath: "/login",
  },
  recipeList: [EmailPassword.init(), Session.init()],
});

const router = ReactRouterDom.createBrowserRouter([
  ...getSuperTokensRoutesForReactRouterDom(ReactRouterDom, [
    EmailPasswordPreBuiltUI,
  ]).map((r) => r.props),
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/protected",
    element: (
      <SessionAuth>
        <ProtectedPage />
      </SessionAuth>
    ),
  },
]);

const App = () => {
  return (
    <ThemeProvider defaultTheme="dark" storageKey="vite-ui-theme">
      <SuperTokensWrapper>
        <ReactRouterDom.RouterProvider router={router} />
      </SuperTokensWrapper>
    </ThemeProvider>
  );
};

export default App;
