import { z } from "zod";

export const env = z
  .object({
    VITE_SUPERTOKENS_APP_NAME: z.string(),
    VITE_SUPERTOKENS_API_DOMAIN: z.string(),
    VITE_SUPERTOKENS_WEBSITE_DOMAIN: z.string(),
    VITE_BASE_URL: z.string(),
  })
  .parse(import.meta.env);
