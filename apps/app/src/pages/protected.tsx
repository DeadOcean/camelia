import { useNavigate } from "react-router-dom";
import { signOut } from "supertokens-auth-react/recipe/emailpassword";
import { typeboxResolver } from "@hookform/resolvers/typebox";
import { createProduct } from "@camelia/dtos";
import type { Static } from "@sinclair/typebox";
import { useForm, type SubmitHandler } from "react-hook-form";

import { Button } from "@/components/ui/button";
import { Card } from "@/components/ui/card";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";

const ProtectedPage = () => {
  const navigate = useNavigate();

  const onSignOut = async () => {
    await signOut();
    navigate("/");
  };

  const form = useForm<Static<typeof createProduct>>({
    resolver: typeboxResolver(createProduct),
  });

  const onSubmit: SubmitHandler<Static<typeof createProduct>> = (values) => {
    console.log(values);
  };

  return (
    <div className="border-1 md:w-4/12 w-11/12 mx-auto">
      <Card className="space-y-8 p-4">
        <Button onClick={onSignOut}>Sign Out</Button>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
            <FormField
              control={form.control}
              name="name"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Nombre</FormLabel>
                  <FormControl>
                    <Input {...field} placeholder="Ej. Infusión de Rooibos" />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <Button type="submit">Guardar</Button>
          </form>
        </Form>
      </Card>
    </div>
  );
};

export default ProtectedPage;
