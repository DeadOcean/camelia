import { Link } from "react-router-dom";

const HomePage = () => (
  <div>
    <Link to="/protected">Protected</Link>
  </div>
);

export default HomePage;
